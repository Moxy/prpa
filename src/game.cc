#include "game.hh"
#include <iostream>


Game::Game(Grid* grid)
{
    current_grid_ = new Grid (*grid);
    future_grid_ = new Grid(*grid);
}

Game::~Game()
{
}

void Game::compute_new_cell(size_t i, size_t j) const
{
    int alive = 0;
    int dead = 0;
    bool status = false;

    std::vector<std::vector<Cell* > >& curr_grid = this->current_grid_->get_grid();
	    
    status = curr_grid[i][j]->get_alive();
    int rows = this->current_grid_->get_rows();
    int cols = this->current_grid_->get_columns();

    if (curr_grid[(i - 1) % rows][(j - 1) % cols]->get_alive() == true)
	alive++;
    else
	dead++;
    
    if (curr_grid[(i - 1) % rows][j % cols]->get_alive() == true)
	alive++;
    else
	dead++;
    
    if (curr_grid[(i - 1) % rows][(j + 1) % cols]->get_alive() == true)
	alive++;
    else
	dead++;
    
    if (curr_grid[i % rows][(j - 1) % cols]->get_alive() == true)
	alive++;
    else
	dead++;
    
    if (curr_grid[(i - 1) % rows][(j + 1) % cols]->get_alive() == true)
	alive++;
    else
	dead++;
    
    if (curr_grid[(i + 1) % rows][(j - 1) % cols]->get_alive() == true)
	alive++;
    else
	dead++;
    
    if (curr_grid[(i + 1) % rows][j % cols]->get_alive() == true)
	alive++;
    else
	dead++;
    
    if (curr_grid[(i + 1) % rows][(j + 1) % cols]->get_alive() == true)
	alive++;
    else
	dead++;
    
    if (status)
    {
	if (alive == 2 or alive == 3)
	    this->future_grid_->get_grid()[i][j]->set_alive(true);
	else
	    this->future_grid_->get_grid()[i][j]->set_alive(false);
    }
    else
    {
	if (alive == 3)
	    this->future_grid_->get_grid()[i][j]->set_alive(true);
	else
	    this->future_grid_->get_grid()[i][j]->set_alive(false);
    }
    alive = 0;
    dead = 0;
}


void Game::operator()(const blocked_range2d<size_t>& range) const
{
    for (size_t i = range.rows().begin(); i != range.rows().end(); i++)
    {
	for (size_t j = range.cols().begin(); j != range.cols().end(); j++)
	{
	    compute_new_cell(i, j);
	}
    }
}

void Game::compute(int start_row, int start_column, int rows, int cols) const
{
    for (int i = start_row; i != rows; i++)
    {
	for (int j = start_column; j != cols; j++)
	{
	    compute_new_cell(i, j);
	}
    }
}

void Game::compute_boost_thread(int start_row, int start_column, int rows, int cols, boost::barrier& curr_barr) const
{
    for (int i = start_row; i != rows; i++)
    {
	for (int j = start_column; j != cols; j++)
	{
	    compute_new_cell(i, j);
	}
    }
    curr_barr.wait();
}


Grid* Game::get_current_grid()
{
    return this->current_grid_;
}

void Game::set_current_grid(Grid* grid)
{
    this->current_grid_ = grid;
}

Grid* Game::get_future_grid()
{
    return this->future_grid_;
}

void Game::set_future_grid(Grid* grid)
{
    this->future_grid_ = grid;
}

void Game::set_current_copy_grid(Grid* grid)
{
    this->current_grid_ = new Grid (*grid);
}

void Game::set_future_copy_grid(Grid* grid)
{
    this->future_grid_ = new Grid (*grid);
}
