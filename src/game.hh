#ifndef GAME_HH_
#define GAME_HH_

#include "tbb/blocked_range2d.h"
#include "tbb/parallel_for.h"
#include "grid.hh"
#include <boost/thread/barrier.hpp>
using namespace tbb;

class Game
{
    private:
	Grid* current_grid_;
	Grid* future_grid_;

    public:
	Game(Grid* grid);
	~Game();
	Grid* get_current_grid();
	void  set_current_grid(Grid* grid);
	Grid* get_future_grid();
	void  set_future_grid(Grid* grid);
	void  set_current_copy_grid(Grid* grid);
	void  set_future_copy_grid(Grid* grid);
	void  compute_new_cell(size_t i, size_t j) const;
	void  operator()(const blocked_range2d<size_t>& range1) const;
        void  compute(int start_row, int start_column, int rows,
		      int cols) const;
	void  compute_boost_thread(int start_row, int start_column,
					int rows, int cols,
				   boost::barrier& curr_barr) const;
};

#endif /* GRID_HH_ */
