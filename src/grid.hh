#ifndef GRID_HH_
#define GRID_HH_

#include <vector>
#include "cell.hh"
#include <fstream>      // std::ofstream



class Grid
{
    private:
	int                              rows_;
	int                              columns_;
	std::vector<std::vector<Cell*> > grid_;

    public:
	Grid(int row, int column);
	Grid(const Grid &grid);
	int get_rows();
	void set_rows(int row);
	int get_columns();
	void set_columns(int column);
	std::vector<std::vector<Cell*> >& get_grid();
	void set_grid(std::vector<std::vector<Cell*> > grid);
	void fill_grid(int row, int column, Cell* cell);
        void print_grid(std::ofstream& of);
};

#endif /* GRID_HH_ */
