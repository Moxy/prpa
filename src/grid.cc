#include <iostream>
#include "grid.hh"

Grid::Grid(int rows, int columns) : rows_ (rows), columns_ (columns)
{
    this->grid_.resize(this->rows_);
    for (int i = 0; i < this->rows_; i++)
    {
	this->grid_[i].resize(this->columns_);
    }
}

Grid::Grid(const Grid &grid)
{
    this->rows_ = grid.rows_;
    this->columns_ = grid.columns_;
    
    this->grid_.resize(this->rows_);


    for (int i = 0; i < this->rows_; i++)
    {
	this->grid_[i].resize(this->columns_);
    }

    for (int i = 0; i < this->rows_; i++)
    {
    	for (int j = 0; j < this->columns_; j++)
    	{
    	    this->fill_grid(i, j, new Cell(grid.grid_[i][j]->get_alive()));
    	}
    }
}

int Grid::get_rows()
{
    return this->rows_;
}

void Grid::set_rows(int rows)
{
    this->rows_ = rows;
}

int Grid::get_columns()
{
    return this->columns_;
}

void Grid::set_columns(int columns)
{
    this->columns_ = columns;
}

std::vector<std::vector<Cell*> >& Grid::get_grid()
{
    return this->grid_;
}

void Grid::set_grid(std::vector<std::vector<Cell*> > grid)
{
    this->grid_ = grid;
}

void Grid::fill_grid(int row, int column, Cell* cell)
{
    this->grid_[row][column] = cell;
}

void Grid::print_grid(std::ofstream& cout)
{
    for (int i = 0; i < this->rows_; i++)
    {
	for (int j = 0; j < this->columns_; j++)
	{    
	    cout << std::noboolalpha << this->grid_[i][j]->get_alive() << " ";
	}
	cout << std::endl;
    }
}
