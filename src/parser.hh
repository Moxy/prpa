#ifndef PARSER_HH
#define PARSER_HH

#include <string>
#include <iostream>
#include <fstream>
#include "grid.hh"

class Parser
{
public:
    Parser(std::string filename);
    int get_column();
    int get_row();
    Grid* get_grid();
    void parse();
private:
    std::string       filename_;
    Grid*             grid_;
    int               column_;
    int               row_;
};

#endif /* PARSER_HH */
