#include "cell.hh"

Cell::Cell(bool alive) : alive_ (alive)
{
}

bool Cell::get_alive()
{
    return this->alive_;
}

void Cell::set_alive(bool value)
{
    this->alive_ = value;
}
