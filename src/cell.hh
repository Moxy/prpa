#ifndef CELL_HH_
#define CELL_HH_

class Cell
{
    private:
	bool alive_;

    public:
	Cell(bool alive);
	bool get_alive();
	void set_alive(bool value);
};

#endif /* CELL_HH_ */
