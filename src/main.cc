#include "parser.hh"
#include "game.hh"
#include <list>
#include <thread>
#include <iostream>
#include <boost/thread.hpp>
#include <tbb/tick_count.h>


void get_divisor(std::list<int>* l, int row, int column)
{
    int max = (row > column)? row : column;

    for (int i = 1; i <= max; ++i)
    {
	if (row % i == 0|| column % i== 0)
	{
	    l->push_back(i);
	}
    }
}


float compute_thread(int n, int rows, int cols, Game* game)
{

    tick_count          before;
    tick_count          after;
    int                 range;
    boost::thread_group g;
    boost::barrier      bar(n);

    if (rows % n == 0)
    {
	range = rows / n;
	before = tick_count::now();
	for (int i = 0; i < n; i++)
	    g.create_thread(boost::bind(&Game::compute_boost_thread, game, i * range, 0,
					range * (i + 1), cols, boost::ref(bar)));
    }
    else
    {
	range = cols / n;
	before = tick_count::now();
	for (int i = 0; i < n; i++)
	    g.create_thread(boost::bind(&Game::compute_boost_thread, game, 0, i * range,
					rows, range * (i + 1), boost::ref(bar)));
    }
    g.join_all();
    after = tick_count::now();
    //std::cout << "time spent (with " << n << " boost thread): "
//	      <<  (after - before).seconds() << " seconds" << std::endl;

    return (after - before).seconds();
}

int main(int argc, char** argv)
{
    if (argc < 2)
	std::cerr << "missing arguments" << std::endl;

    Parser* parser = new Parser(argv[1]);
    parser->parse();
    
    Game* game = 0;
    Game* game_boost = 0;


    int rows = parser->get_row();
    int cols =  parser->get_column();

    tick_count before;
    tick_count after;
    int number_it = 1;
    float boost_avg = 0;


    std::vector<float> serial_res;
    std::vector<float> tbb_res;
    std::vector<float> boost_tmp_res;
    std::vector<float> boost_res;
    std::ofstream&& boost_out = std::ofstream("boost_out");
    std::ofstream&& paral_out = std::ofstream("paral_out");
    std::ofstream&& ofs = std::ofstream("out");

    argc = argc;
    if (argc > 2) 
	number_it = atoi(argv[2]);
       
    std::list<int> l;
    get_divisor(&l, rows, cols);

    game = new Game(parser->get_grid());

    for (int j = 0; j < number_it; j++)
    {	
	before = tick_count::now();
	game->compute(0, 0 ,rows, cols);
	game->get_future_grid()->print_grid(ofs);
	after = tick_count::now();
	serial_res.push_back((after - before).seconds());
//	std::cout << "time spent (without parallelism): " <<  (after - before).seconds() << " seconds" << std::endl;

	before = tick_count::now();
	parallel_for(blocked_range2d<size_t>(0, rows, 32, 0, cols, 32), *game);
	game->get_future_grid()->print_grid(paral_out);
	after = tick_count::now();	
	tbb_res.push_back((after - before).seconds());
//	std::cout << "time spent (with tbb parallel for): " <<  (after - before).seconds() << " seconds" << std::endl;

	game->set_current_copy_grid(game->get_future_grid());
    }
    bool is_print = false;
    for (auto n : l)
    {
	game_boost = new Game(parser->get_grid());

	for (int j = 0; j < number_it; j++)
	{
	    boost_tmp_res.push_back(compute_thread(n, rows, cols, game_boost));
	    boost_avg = std::accumulate(boost_tmp_res.begin(), boost_tmp_res.end(), 0.0) / boost_tmp_res.size();
	    boost_res.push_back(boost_avg);
	    boost_tmp_res.clear();
	    game_boost->set_current_copy_grid(game_boost->get_future_grid());
	    if (!is_print)
		game_boost->get_future_grid()->print_grid(boost_out);
	}
	is_print = true;
	delete(game_boost);
    }
    delete(game);

    float serial_avg =  std::accumulate(serial_res.begin(), serial_res.end(), 0.0) / serial_res.size();
    float tbb_avg =  std::accumulate(tbb_res.begin(), tbb_res.end(), 0.0) / tbb_res.size();

    std::cout << std::endl;
    std::cout << "---------------------------------------------------" << std::endl;
    std::cout << "Average time spent (without parallelism): " <<  serial_avg << " seconds" << std::endl;
    std::cout << "Average time spent (with tbb parallel for): " <<  tbb_avg << " seconds" << std::endl;
    
    int index_boost_res = 0;

    for (auto n : l)
    {
	std::cout << "Average time spent (with " << n << " boost thread): "
		  <<  boost_res[index_boost_res] << " seconds" << std::endl;
	index_boost_res++;
    }

    //game->get_current_grid()->print_grid();
    //game->get_future_grid()->print_grid();
    ofs.close();
    boost_out.close();
    paral_out.close();

    return 0;

}
