#include "parser.hh"

Parser::Parser(std::string filename) :
    filename_(filename), grid_(0), column_(0), row_(0)
{
} 

void Parser::parse()
{
    std::ifstream ifs;
    bool          alive;
    char          tmp[4];
    char          c;

    ifs.open (filename_, std::ifstream::in);
    
    if (ifs.good())
    {
	ifs.read(tmp, 3);
	tmp[3] = '\0';
	row_ = atoi(tmp);
	ifs.read(tmp, 2);
	tmp[2] = '\0';
	column_ = atoi(tmp);
	grid_ = new Grid(row_, column_);
    }
    else
	std::cerr << "Invalid input file" << std::endl;

    for (int i = 0; i < row_; ++i)
    {
	for (int j = 0; j < column_; ++j)
	{
	    if (ifs.good())
	    {
		ifs >> c;
		if (c == '1')
		    alive = true;
		else
		    alive = false;
		grid_->fill_grid(i, j, new Cell(alive));
	    }
	    else
		std::cerr << "Invalid input at row " << j
			  << "and column " << i << std::endl; 
	}
    }
}

int Parser::get_column()
{
    return column_;
}

int Parser::get_row()
{
    return row_;
}

Grid* Parser::get_grid()
{
    return grid_;
}
