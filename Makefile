include Makefile.in

all:
	$(CC) $(CFLAGS) src/cell.cc src/grid.cc src/parser.cc src/game.cc src/main.cc $(LDFLAGS)	
.PHONY: test
test: 
	./test.sh
