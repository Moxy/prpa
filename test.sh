#!/bin/bash

red='\e[0;31m'
blue='\e[1;34m'
NC='\e[0m'

echo -e "${blue}#######################################################################${NC}"
echo -e "${blue}######################## GAME OF LIFE BENCHMARK #######################${NC}"
echo -e "${blue}#######################################################################${NC}"
echo 
echo
echo -e "${red}\e[5mTEST 1: ROW 10, COLUMN 10 ${NC}"
echo
./a.out test/t10x10 100 
echo
echo
echo -e "${red}\e[5mTEST 2: ROW 20, COLUMN 30 ${NC}"
./a.out test/t20x30 100
echo
echo
echo -e "${red}\e[5mTEST 3: ROW 50, COLUMN 40 ${NC}"
echo
./a.out test/t50x40 100
echo
echo
echo -e "${red}\e[5mTEST 3: ROW 100, COLUMN 100 ${NC}"
echo
./a.out test/t100x100 1


